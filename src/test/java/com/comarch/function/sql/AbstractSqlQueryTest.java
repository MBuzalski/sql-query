package com.comarch.function.sql;

import com.comarch.function.Value;
import org.junit.*;
import org.junit.rules.ExpectedException;
import org.slf4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public abstract class AbstractSqlQueryTest {

    protected SqlQuery sqlQuery;
    protected String query;
    static protected Connection c = null;
    protected static Logger LOGGER;
    protected String type;


    @Rule
    public final ExpectedException exception = ExpectedException.none();


    @BeforeClass
    public static void initialize() {
        try {
            c = DriverManager.getConnection("jdbc:sqlite:src/test/resources/test.db");
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException | SQLException e) {
            LOGGER.error("{}: {}", e.getClass().getName(), e.getMessage());
        }
    }

    @Test
    public void invokeZeroArgInteger() throws Exception {
        query = "SELECT LICZBA FROM TESTTABLE WHERE TEKST='jeden'";
        sqlQuery = SqlQueryFactory.create(c, query, type);
        Value v = sqlQuery.invoke();

        Assert.assertEquals(Value.valueOf(1), v);
    }

    @Test
    public void invokeZeroArgText() throws Exception {

        query = "SELECT TEKST FROM TESTTABLE WHERE PRZECINKOWA=1.1";
        sqlQuery = SqlQueryFactory.create(c, query, type);
        Value v = sqlQuery.invoke();

        Assert.assertEquals(Value.valueOf("jeden"), v);
    }

    @Test
    public void invokeZeroReal() throws Exception {

        query = "SELECT PRZECINKOWA FROM TESTTABLE WHERE LICZBA=1";
        sqlQuery = SqlQueryFactory.create(c, query, type);
        Value v = sqlQuery.invoke();

        Assert.assertEquals(Value.valueOf(1.1), v);
    }

    @Test
    public void invokeZeroArgNothingFound() throws Exception {

        query = "SELECT TEKST FROM TESTTABLE WHERE LICZBA=1768";
        sqlQuery = SqlQueryFactory.create(c, query, type);
        Value v = sqlQuery.invoke();

        Assert.assertEquals(Value.EMPTY_VALUE, v);
    }

    @Test
    public void invokeSingleNumberToken() throws Exception {

        query = "SELECT TEKST FROM TESTTABLE WHERE LICZBA=:1";
        sqlQuery = SqlQueryFactory.create(c, query, type);
        Value v = sqlQuery.invoke(Value.valueOf(4));

        Assert.assertEquals(Value.valueOf("cztery"), v);
    }

    @Test
    public void invokeSingleTextTokenUsedTwoTimes() throws Exception {

        query = "SELECT liczba FROM TESTTABLE WHERE liczba<:1 AND przecinkowa<:1";
        sqlQuery = SqlQueryFactory.create(c, query, type);
        Value v = sqlQuery.invoke(Value.valueOf("10"));

        Assert.assertEquals(Value.valueOf(6), v);
    }

    @Test
    public void invokeMultipleTokens() throws Exception {

        query = "SELECT tekst FROM TESTTABLE WHERE liczba>:1 AND liczba <:2";
        sqlQuery = SqlQueryFactory.create(c, query, type);
        Value v = sqlQuery.invoke(Value.valueOf(4), Value.valueOf("8"));

        Assert.assertEquals(Value.valueOf("szesc"), v);
    }

    @Test
    public void invokeMultipleUses() throws Exception {

        query = "SELECT TEKST FROM TESTTABLE WHERE LICZBA=:1";
        sqlQuery = SqlQueryFactory.create(c, query, type);

        exception.expect(IllegalStateException.class);
        exception.expectMessage("Invoke can only be used once per SqlQuery object");
        sqlQuery.invoke(Value.valueOf(6));
        sqlQuery.invoke();
    }

    @Test
    public void invokeEmptyQuery() throws Exception {

        query = "";
        sqlQuery = SqlQueryFactory.create(c, query, type);
        Value v = sqlQuery.invoke();

        Assert.assertEquals(Value.EMPTY_VALUE, v);
        LOGGER.info("invokeEmptyQuery");
    }

    @AfterClass
    public static void tearDown() throws Exception {
        c.close();
    }

}