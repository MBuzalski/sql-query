package com.comarch.function.sql.modifier;

import com.comarch.function.Value;
import com.comarch.function.sql.modifier.CustomSqlQueryModifier;
import com.comarch.function.sql.modifier.SqlQueryModifier;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.security.InvalidParameterException;
import java.util.ArrayList;

public class CustomSqlQueryModifierTest {

    private SqlQueryModifier queryModifier;

    @Rule
    public final ExpectedException exception = ExpectedException.none();


    @Test
    public void processSimpleQuery() throws Exception {
        queryModifier = new CustomSqlQueryModifier("var");
        String query = "SELECT whatever FROM table WHERE Age=var1 AND Height>var2 AND Height<var3";
        String result = "SELECT whatever FROM table WHERE Age=18.0 AND Height>160.5 AND Height<195.5";

        Assert.assertEquals(result, queryModifier.processSimpleQuery(query, Value.valueOf(18), Value.valueOf(160.5), Value.valueOf(195.5)));
    }

    @Test
    public void processSimpleQueryInvalidTokens() throws Exception {
        queryModifier = new CustomSqlQueryModifier("?");
        String query = "SELECT przecinkowa FROM TESTTABLE WHERE ?1=?3";

        exception.expect(InvalidParameterException.class);
        exception.expectMessage("Token numbers do not match function arguments");
        queryModifier.processSimpleQuery(query, Value.valueOf(5), Value.valueOf(16.6));
    }

    @Test
    public void processSafeQuery() throws Exception {
        queryModifier = new CustomSqlQueryModifier("#");
        String query = "SELECT whatever FROM table WHERE Age=#1 AND Height>#2 AND Height<#3";
        String result = "SELECT whatever FROM table WHERE Age=? AND Height>? AND Height<?";

        Assert.assertEquals(result, queryModifier.processSafeQuery(query, 3));
    }

    @Test
    public void processSafeQueryInvalidTokens() throws Exception {
        queryModifier = new CustomSqlQueryModifier("@");
        String query = "SELECT przecinkowa FROM TESTTABLE WHERE @1=@3";

        exception.expect(InvalidParameterException.class);
        exception.expectMessage("Token numbers do not match function arguments");
        queryModifier.processSimpleQuery(query, Value.valueOf(5), Value.valueOf(16.6));
    }

    @Test
    public void getTokenList() throws Exception {
        queryModifier = new CustomSqlQueryModifier("token");
        String query = "SELECT whatever FROM table WHERE Age=token1 AND Height>token2 AND Height<token3";
        ArrayList<String> tokenList = new ArrayList<>();
        tokenList.add("1");
        tokenList.add("2");
        tokenList.add("3");

        Assert.assertEquals(tokenList, queryModifier.getTokenList(query));
    }

    @Test
    public void processSimpleQueryMetaCharacterStar() throws Exception {
        queryModifier = new CustomSqlQueryModifier("*");
        String query = "SELECT whatever FROM table WHERE Age=*1 AND Height>*2 AND Height<*3";
        String result = "SELECT whatever FROM table WHERE Age=18.0 AND Height>160.5 AND Height<195.5";

        Assert.assertEquals(result, queryModifier.processSimpleQuery(query, Value.valueOf(18), Value.valueOf(160.5), Value.valueOf(195.5)));
    }

    @Test
    public void processSafeQueryMetaCharacterHigherThan() throws Exception {
        queryModifier = new CustomSqlQueryModifier(">");
        String query = "SELECT whatever FROM table WHERE Age=>1 AND Height>>2 AND Height<>3";
        String result = "SELECT whatever FROM table WHERE Age=? AND Height>? AND Height<?";

        Assert.assertEquals(result, queryModifier.processSafeQuery(query, 3));
    }

    @Test
    public void getTokenListMetaCharacterEqual() throws Exception {
        queryModifier = new CustomSqlQueryModifier("=");
        String query = "SELECT whatever FROM table WHERE Age==1 AND Height>=2 AND Height<=3";
        ArrayList<String> tokenList = new ArrayList<>();
        tokenList.add("1");
        tokenList.add("2");
        tokenList.add("3");

        Assert.assertEquals(tokenList, queryModifier.getTokenList(query));
    }


}