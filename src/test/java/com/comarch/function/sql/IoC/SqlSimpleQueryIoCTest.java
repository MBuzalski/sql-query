package com.comarch.function.sql.IoC;

        import com.comarch.function.Value;
        import com.comarch.function.sql.SqlQFactory;
        import com.comarch.function.sql.SqlSafeQueryTest;
        import com.comarch.function.sql.modules.SqlSimpleQueryModule;
        import com.google.inject.Guice;
        import org.junit.AfterClass;
        import org.junit.Assert;
        import org.junit.BeforeClass;
        import org.junit.Test;
        import org.slf4j.LoggerFactory;

        import java.sql.Connection;


public class SqlSimpleQueryIoCTest extends AbstractSqlQueryIoCTest{

    @BeforeClass
    public static void setUpFirst()
    {
        injector = Guice.createInjector(new SqlSimpleQueryModule());
        factory = injector.getInstance(SqlQFactory.class);
        LOGGER = LoggerFactory.getLogger(SqlSafeQueryTest.class);
        c = injector.getInstance(Connection.class);
    }

    @Test
    public void invokeTextTokenUsedAsColumnName() throws Exception {

        query = "SELECT :1 FROM TESTTABLE WHERE :1>6 AND :1<15";
        sqlQuery = factory.create(query);
        Value v = sqlQuery.invoke(Value.valueOf("liczba"));

        Assert.assertEquals(Value.valueOf(9), v);
    }

    @AfterClass
    public static void tearDown() throws Exception {
        c.close();
    }
}
