package com.comarch.function.sql.IoC;

import com.comarch.function.Value;
import com.comarch.function.sql.SqlQFactory;
import com.comarch.function.sql.SqlQuery;
import com.google.inject.Inject;
import com.google.inject.Injector;
import org.junit.*;
import org.junit.rules.ExpectedException;
import org.slf4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public abstract class AbstractSqlQueryIoCTest {

    protected SqlQuery sqlQuery;
    protected String query;
    static protected Connection c = null;
    protected static Logger LOGGER;
    @Inject
    static protected SqlQFactory factory;
    static protected Injector injector;


    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void invokeZeroArgInteger() throws Exception {
        query = "SELECT LICZBA FROM TESTTABLE WHERE TEKST='jeden'";
        sqlQuery=factory.create(query);
        Value v = sqlQuery.invoke();

        Assert.assertEquals(Value.valueOf(1), v);
    }

    @Test
    public void invokeZeroArgText() throws Exception {

        query = "SELECT TEKST FROM TESTTABLE WHERE PRZECINKOWA=1.1";
        sqlQuery=factory.create(query);
        Value v = sqlQuery.invoke();

        Assert.assertEquals(Value.valueOf("jeden"), v);
    }

    @Test
    public void invokeZeroReal() throws Exception {

        query = "SELECT PRZECINKOWA FROM TESTTABLE WHERE LICZBA=1";
        sqlQuery=factory.create(query);
        Value v = sqlQuery.invoke();

        Assert.assertEquals(Value.valueOf(1.1), v);
    }

    @Test
    public void invokeZeroArgNothingFound() throws Exception {

        query = "SELECT TEKST FROM TESTTABLE WHERE LICZBA=1768";
        sqlQuery=factory.create(query);
        Value v = sqlQuery.invoke();

        Assert.assertEquals(Value.EMPTY_VALUE, v);
    }

    @Test
    public void invokeSingleNumberToken() throws Exception {

        query = "SELECT TEKST FROM TESTTABLE WHERE LICZBA=:1";
        sqlQuery=factory.create(query);
        Value v = sqlQuery.invoke(Value.valueOf(4));

        Assert.assertEquals(Value.valueOf("cztery"), v);
    }

    @Test
    public void invokeSingleTextTokenUsedTwoTimes() throws Exception {

        query = "SELECT liczba FROM TESTTABLE WHERE liczba<:1 AND przecinkowa<:1";
        sqlQuery=factory.create(query);
        Value v = sqlQuery.invoke(Value.valueOf("10"));

        Assert.assertEquals(Value.valueOf(6), v);
    }

    @Test
    public void invokeMultipleTokens() throws Exception {

        query = "SELECT tekst FROM TESTTABLE WHERE liczba>:1 AND liczba <:2";
        sqlQuery=factory.create(query);
        Value v = sqlQuery.invoke(Value.valueOf(4), Value.valueOf("8"));

        Assert.assertEquals(Value.valueOf("szesc"), v);
    }

    @Test
    public void invokeMultipleUses() throws Exception {

        query = "SELECT TEKST FROM TESTTABLE WHERE LICZBA=:1";
        sqlQuery=factory.create(query);

        exception.expect(IllegalStateException.class);
        exception.expectMessage("Invoke can only be used once per SqlQuery object");
        sqlQuery.invoke(Value.valueOf(6));
        sqlQuery.invoke();
    }

    @Test
    public void invokeEmptyQuery() throws Exception {

        query = "";
        sqlQuery=factory.create(query);
        Value v = sqlQuery.invoke();

        Assert.assertEquals(Value.EMPTY_VALUE, v);
        LOGGER.info("invokeEmptyQuery");
    }

}
