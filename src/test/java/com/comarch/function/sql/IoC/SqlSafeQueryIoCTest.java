package com.comarch.function.sql.IoC;

import com.comarch.function.sql.SqlQFactory;
import com.comarch.function.sql.SqlSafeQueryTest;
import com.comarch.function.sql.modules.SqlSafeQueryModule;
import com.google.inject.Guice;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.slf4j.LoggerFactory;

import java.sql.Connection;


public class SqlSafeQueryIoCTest extends AbstractSqlQueryIoCTest{

    @BeforeClass
    public static void setUpFirst()
    {
        injector = Guice.createInjector(new SqlSafeQueryModule());
        factory = injector.getInstance(SqlQFactory.class);
        LOGGER = LoggerFactory.getLogger(SqlSafeQueryTest.class);
        c = injector.getInstance(Connection.class);
    }

    @AfterClass
    public static void tearDown() throws Exception {
        c.close();
    }

}
