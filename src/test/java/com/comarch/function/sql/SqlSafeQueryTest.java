package com.comarch.function.sql;

import org.junit.Before;
import org.slf4j.LoggerFactory;


public class SqlSafeQueryTest extends AbstractSqlQueryTest {

    @Before
    public void setUp() {
        type = "safe";
        LOGGER = LoggerFactory.getLogger(SqlSafeQueryTest.class);

    }

}