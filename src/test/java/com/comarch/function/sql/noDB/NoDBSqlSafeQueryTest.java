package com.comarch.function.sql.noDB;

import com.comarch.function.Value;
import com.comarch.function.sql.SqlQueryFactory;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

import static org.mockito.Matchers.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


public class NoDBSqlSafeQueryTest extends AbstractNoDBSqlQueryTest {

    private PreparedStatement stmt = Mockito.mock(PreparedStatement.class);
    private static final Logger LOGGER = LoggerFactory.getLogger(NoDBSqlSafeQueryTest.class);

    @Before
    public void setUp() {
        query = "SELECT TEKST FROM TESTTABLE WHERE LICZBA=:1";
        try {

            when(stmt.executeQuery()).thenReturn(null);
            when(c.prepareStatement(anyString())).thenReturn(stmt);
            when(converter.convert(any())).thenReturn(Value.EMPTY_VALUE);
            when(queryModifier.processSafeQuery(anyString(), anyInt())).thenReturn("");
            when(queryModifier.getTokenList(anyString())).thenReturn(new ArrayList<String>());

            sqlQuery = SqlQueryFactory.create(c, query, "safe");
            sqlQuery.setQueryModifier(queryModifier);
            sqlQuery.setResultSetToValueConverter(converter);

        } catch (SQLException e) {
            LOGGER.error("{}: {}", e.getClass().getName(), e.getMessage());
        }
    }

    @Test
    public void invokeCalledGetTokenList() {
        sqlQuery.invoke(Value.valueOf(6));
        verify(queryModifier, times(1)).getTokenList(anyString());
    }

    @Test
    public void invokeCalledProcessSafeQuery() {
        sqlQuery.invoke(Value.valueOf(6));
        verify(queryModifier, times(1)).processSafeQuery(anyString(), anyInt());
    }

    @Test
    public void invokeCalledPrepareStatement() throws SQLException {
        sqlQuery.invoke(Value.valueOf(6));
        verify(c, times(1)).prepareStatement(anyString());

    }

    @Test
    public void invokeCalledExecuteQuery() throws SQLException {
        sqlQuery.invoke(Value.valueOf(6));
        verify(stmt).executeQuery();

    }

    @Test
    public void invokeCalledConvert() throws SQLException {
        sqlQuery.invoke(Value.valueOf(6));
        verify(converter).convert(any());

    }

}
