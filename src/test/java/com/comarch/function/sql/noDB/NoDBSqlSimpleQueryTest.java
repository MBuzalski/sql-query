package com.comarch.function.sql.noDB;

import com.comarch.function.Value;
import com.comarch.function.sql.SqlQueryFactory;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.sql.Statement;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.anyVararg;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class NoDBSqlSimpleQueryTest extends AbstractNoDBSqlQueryTest {

    private Statement stmt = Mockito.mock(Statement.class);
    private static final Logger LOGGER = LoggerFactory.getLogger(NoDBSqlSimpleQueryTest.class);

    @Before
    public void setUp() {
        query = "SELECT TEKST FROM TESTTABLE WHERE LICZBA=:1";
        try {
            when(c.createStatement()).thenReturn(stmt);
            when(stmt.executeQuery(anyString())).thenReturn(null);
            when(converter.convert(any())).thenReturn(Value.EMPTY_VALUE);
            when(queryModifier.processSimpleQuery(anyString())).thenReturn("");

            sqlQuery = SqlQueryFactory.create(c, query, "simple");
            sqlQuery.setQueryModifier(queryModifier);
            sqlQuery.setResultSetToValueConverter(converter);

        } catch (SQLException e) {
            LOGGER.error("{}: {}", e.getClass().getName(), e.getMessage());
        }
    }

    @Test
    public void invokeCalledProcessSimpleQuery() {
        sqlQuery.invoke(Value.valueOf(6));
        verify(queryModifier).processSimpleQuery(anyString(), anyVararg());
    }

    @Test
    public void invokeCalledCreateStatement() throws SQLException {
        sqlQuery.invoke(Value.valueOf(6));
        verify(c).createStatement();

    }

    @Test
    public void invokeCalledExecuteQuery() throws SQLException {
        sqlQuery.invoke(Value.valueOf(6));

        verify(stmt).executeQuery(anyString());

    }

    @Test
    public void invokeCalledConvert() throws SQLException {
        sqlQuery.invoke(Value.valueOf(6));

        verify(converter).convert(any());

    }
}
