package com.comarch.function.sql.noDB;

import com.comarch.function.Value;
import com.comarch.function.sql.SqlQuery;
import com.comarch.function.sql.converter.DefaultResultSetToValueConverter;
import com.comarch.function.sql.converter.ResultSetToValueConverter;
import com.comarch.function.sql.modifier.DefaultSqlQueryModifier;
import com.comarch.function.sql.modifier.SqlQueryModifier;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;

import java.sql.Connection;

public abstract class AbstractNoDBSqlQueryTest {

    protected SqlQuery sqlQuery;
    protected Connection c = Mockito.mock(Connection.class);
    protected ResultSetToValueConverter converter = Mockito.mock(DefaultResultSetToValueConverter.class);
    protected SqlQueryModifier queryModifier = Mockito.mock(DefaultSqlQueryModifier.class);
    protected String query;

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void invokeMultipleUsesSimpleQuery() throws Exception {

        sqlQuery.setResultSetToValueConverter(converter);
        sqlQuery.setQueryModifier(queryModifier);

        exception.expect(IllegalStateException.class);
        exception.expectMessage("Invoke can only be used once per SqlQuery object");
        sqlQuery.invoke(Value.valueOf(6));
        sqlQuery.invoke();
    }


}