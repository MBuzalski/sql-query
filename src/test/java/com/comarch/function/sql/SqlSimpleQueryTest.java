package com.comarch.function.sql;

import com.comarch.function.Value;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.junit.*;
import org.slf4j.LoggerFactory;


public class SqlSimpleQueryTest extends AbstractSqlQueryTest {


    @Before
    public void setUp() {
        type = "simple";
        LOGGER = LoggerFactory.getLogger(SqlSimpleQueryTest.class);


    }

    @Test
    public void invokeTextTokenUsedAsColumnName() throws Exception {

        query = "SELECT :1 FROM TESTTABLE WHERE :1>6 AND :1<15";
        sqlQuery = SqlQueryFactory.create(c, query, type);
        Value v = sqlQuery.invoke(Value.valueOf("liczba"));

        Assert.assertEquals(Value.valueOf(9), v);
    }


}