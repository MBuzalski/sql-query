package com.comarch.function.sql;

import org.junit.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SqlQueryTestIT extends AbstractSqlQueryTestIT {

    private static Connection connection;

    @Before
    public void setUp() {
        try {
            Class.forName("org.postgresql.Driver");
            connection = null;
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "buzalskim", "x");

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    SqlQuery createSqlQueryFor(String query) {

        return SqlQueryFactory.create(connection, query, "simple");
    }


    @After
    public void tearDown() {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}