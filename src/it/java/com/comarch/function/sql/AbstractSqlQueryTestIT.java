package com.comarch.function.sql;

import static org.junit.Assert.assertEquals;

import org.junit.Rule;
import org.junit.Test;

import com.comarch.function.Value;
import org.junit.rules.ExpectedException;

abstract class AbstractSqlQueryTestIT {
    private SqlQuery sqlQuery;


    @Test
    public void testSelectOneFieldWithoutParametersShouldWork() {
        sqlQuery = createSqlQueryFor("select id from staz.c2s_int_structure order by id limit 1");
        assertEquals(Value.valueOf(152261), sqlQuery.invoke());
    }

    abstract SqlQuery createSqlQueryFor(String query);

    @Test(expected = RuntimeException.class)
    public void failWhenInvokedTwice() {
        sqlQuery = createSqlQueryFor("select id from staz.c2s_int_structure order by id limit 1");
        sqlQuery.invoke();
        sqlQuery.invoke();
    }

    @Test
    public void testSelectOneFieldWithOneParameterShouldWork() {
        sqlQuery = createSqlQueryFor("select id from staz.c2s_int_structure where id=:1 limit 1");
        assertEquals(Value.valueOf(152261), sqlQuery.invoke(Value.valueOf(152261)));
    }

    @Test(expected = RuntimeException.class)
    public void failWhenSelectOneFieldWithIncorrectParameter() {
        sqlQuery = createSqlQueryFor("select id from staz.c2s_int_structure where id=:2 limit 1");
        sqlQuery.invoke(Value.EMPTY_VALUE); // any value
    }

    @Test(expected = RuntimeException.class)
    public void failWhenArgumentCountDoesntMatchTokenCount() {
        sqlQuery = createSqlQueryFor("select id from staz.c2s_int_structure where id=:1 limit 1");
        sqlQuery.invoke();
    }

    @Test
    public void testArgumentRepetition() {
        sqlQuery = createSqlQueryFor(
                "select id from staz.c2s_int_structure where id =:2 and leaf =:1 and archive=:1  limit 1");
        sqlQuery.invoke(Value.valueOf("'f'"), Value.valueOf(152261));
    }

    @Test(expected = RuntimeException.class)
    public void testArgumentTokenDiscontinuity() {
        sqlQuery = createSqlQueryFor(
                "select id from staz.c2s_int_structure where id =:3 and leaf =:1 and archive=:1  limit 1");
        sqlQuery.invoke(Value.valueOf("f"), Value.valueOf(152261));
    }

    @Test
    public void testReturnOneFieldMultipleParameters() {
        sqlQuery = createSqlQueryFor("select id from staz.c2s_int_structure where id=:2");
    }

}
