package com.comarch.function;

public interface Function {
    /**
     * Wykonuje funkcje z wykorzystaniem parametrów i zwraca jej wynik
     *
     * @param values - tablic wartości wejściowych
     * @return wartość wyjściowa funkcji
     */
    Value invoke(Value... values);
}
