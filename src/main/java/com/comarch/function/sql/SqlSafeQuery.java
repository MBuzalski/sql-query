package com.comarch.function.sql;

import com.comarch.function.Value;
import com.comarch.function.sql.converter.ResultSetToValueConverter;
import com.comarch.function.sql.modifier.SqlQueryModifier;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.List;

/**
 * Utility class for making parametrized queries to database using PreparedStatement
 * Example:
 * Initial query: SELECT LastName FROM ExampleTable WHERE Age<:1 AND Age>:2
 * Call: sqlQuery.invoke(Value.valueOf(21),Value.valeOf(12));
 * Processed query: SELECT LastNme FROM ExampleTAble WHERE AGE<21 AND AGE>12
 */
public class SqlSafeQuery extends SqlQuery {

    private static final Logger LOGGER = LoggerFactory.getLogger(SqlSafeQuery.class);

    /**
     * @param connection - connection to the database query     *
     * @param query      - initial query string to process
     */
    public SqlSafeQuery(Connection connection, String query) {
        super(connection, query);
    }

    /**
     * Constructor for Guice Dependancy Injection
     */
    @Inject
    public SqlSafeQuery(Connection connection, @Assisted String query,
                          SqlQueryModifier queryModifier,
                          ResultSetToValueConverter resultSetToValueConverter) {
        super(connection, query);
        this.queryModifier=queryModifier;
        this.resultSetToValueConverter=resultSetToValueConverter;
    }

    /**
     * @param values - array of values for token replacement
     * @return Value object containing database query result
     */
    @Override
    public Value invoke(Value... values) {

        alreadyUsed();

        Value value = Value.EMPTY_VALUE;

        List<String> tokenList = queryModifier.getTokenList(query);
        String finalQuery = queryModifier.processSafeQuery(query, values.length);

        try (PreparedStatement statement = connection.prepareStatement(finalQuery)) {
            int i = 1;
            for (String s : tokenList) {
                Value v = values[Integer.parseInt(s) - 1];
                if (v.getType().equals(Value.Type.NUMBER)) {
                    statement.setObject(i, v, Types.DECIMAL);
                } else {
                    statement.setObject(i, v);
                }

                i++;
            }
            try (ResultSet rs = statement.executeQuery()) {
                value = resultSetToValueConverter.convert(rs);
            }
        } catch (SQLException e) {
            LOGGER.error("{}: {}", e.getClass().getName(), e.getMessage());
        }
        return value;
    }


}
