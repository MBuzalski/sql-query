package com.comarch.function.sql.converter;

import com.comarch.function.Value;

import java.sql.ResultSet;
import java.sql.SQLException;

@FunctionalInterface
public interface ResultSetToValueConverter{
    Value convert(ResultSet rs) throws SQLException;
}
