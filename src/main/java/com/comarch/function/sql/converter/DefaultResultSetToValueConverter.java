package com.comarch.function.sql.converter;

import com.comarch.function.Value;

import java.sql.ResultSet;
import java.sql.SQLException;


public class DefaultResultSetToValueConverter implements ResultSetToValueConverter {

    public Value convert(ResultSet resultSet) throws SQLException {

        if (!resultSet.next()) {
            return Value.EMPTY_VALUE;
        } else {
            int column = 1;
            Object result = resultSet.getObject(column);
            if (result instanceof String) {
                return Value.valueOf(resultSet.getString(column));
            } else if (result instanceof Integer) {
                return Value.valueOf(resultSet.getInt(column));
            } else if (result instanceof Double) {
                return Value.valueOf(resultSet.getDouble(column));
            }
        }

        return Value.EMPTY_VALUE;
    }
}
