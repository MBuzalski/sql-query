package com.comarch.function.sql;

/**
 * created by buzalskim on 2016-07-19.
 * Factory interface for Guice auto-generation
 */
@FunctionalInterface
public interface SqlQFactory {

    SqlQuery create(String query);
}
