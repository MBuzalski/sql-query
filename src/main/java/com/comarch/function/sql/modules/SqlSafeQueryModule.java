package com.comarch.function.sql.modules;

import com.comarch.function.sql.SqlQFactory;
import com.comarch.function.sql.SqlQuery;
import com.comarch.function.sql.SqlSafeQuery;
import com.comarch.function.sql.SqlSimpleQuery;
import com.comarch.function.sql.converter.DefaultResultSetToValueConverter;
import com.comarch.function.sql.converter.ResultSetToValueConverter;
import com.comarch.function.sql.modifier.DefaultSqlQueryModifier;
import com.comarch.function.sql.modifier.SqlQueryModifier;
import com.google.inject.AbstractModule;
import com.google.inject.assistedinject.FactoryModuleBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SqlSafeQueryModule extends AbstractModule {

    private static final String DRIVER = "org.sqlite.JDBC";
    private static final String URL = "jdbc:sqlite:src/main/resources/test.db";
    private static final Logger LOGGER = LoggerFactory.getLogger(SqlSimpleQueryModule.class);

    @Override
    protected void configure() {
        bind(SqlQueryModifier.class).to(DefaultSqlQueryModifier.class);
        bind(ResultSetToValueConverter.class).to(DefaultResultSetToValueConverter.class);

        try {
            Class.forName(DRIVER);
            bind(Connection.class).toInstance(DriverManager.getConnection(URL));
        } catch (SQLException | ClassNotFoundException e) {
            LOGGER.error("{}: {}", e.getClass().getName(), e.getMessage());
        }
        install(new FactoryModuleBuilder()
                .implement(SqlQuery.class, SqlSimpleQuery.class)
                .build(SqlQFactory.class));
    }
}
