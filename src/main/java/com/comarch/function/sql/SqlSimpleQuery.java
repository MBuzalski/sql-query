package com.comarch.function.sql;

import com.comarch.function.Value;
import com.comarch.function.sql.converter.ResultSetToValueConverter;
import com.comarch.function.sql.modifier.SqlQueryModifier;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Utility class for making parametrized queries to database
 * Example:
 * Initial query: SELECT LastName FROM ExampleTable WHERE :1<:2 AND :1>:3
 * Call: sqlQuery.invoke(Value.valueOf("Age"),Value.valueOf(21),Value.valeOf(12));
 * Processed query: SELECT LastNme FROM ExampleTAble WHERE AGE<21 AND AGE>12
 */
public class SqlSimpleQuery extends SqlQuery {

    private static final Logger LOGGER = LoggerFactory.getLogger(SqlSimpleQuery.class);

    /**
     * @param connection - connection to the database query
     * @param query      - initial query string to process
     */
    public SqlSimpleQuery(Connection connection, String query) {
        super(connection, query);
    }

    /**
     * Constructor for Guice Dependancy Injection
     */
    @Inject
    public SqlSimpleQuery(Connection connection, @Assisted String query,
                          SqlQueryModifier queryModifier,
                          ResultSetToValueConverter resultSetToValueConverter) {
        super(connection, query);
        this.queryModifier=queryModifier;
        this.resultSetToValueConverter=resultSetToValueConverter;
    }

    /**
     * @param values - array of values for token replacement
     * @return Value object containing database query result
     */
    @Override
    public Value invoke(Value... values) {

        alreadyUsed();

        Value value = Value.EMPTY_VALUE;

        String finalQuery = queryModifier.processSimpleQuery(query, values);

        try (Statement statement = connection.createStatement();
             ResultSet rs = statement.executeQuery(finalQuery)) {

            value = resultSetToValueConverter.convert(rs);

        } catch (SQLException e) {
            LOGGER.error("{}: {}", e.getClass().getName(), e.getMessage());
        }
        return value;
    }


}
