package com.comarch.function.sql.modifier;

import com.comarch.function.Value;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Default implementation of SqlQueryModifier interface, using ':' as token identifier
 * Created by buzalskim on 2016-07-15.
 */
public class DefaultSqlQueryModifier implements SqlQueryModifier {

    private static Pattern pattern = Pattern.compile(":\\d|:\\d\\d");

    @Override
    public String processSimpleQuery(String query, Value... values) {
        for (int i = 0; i < values.length; i++) {
            query = replaceToken(query, i, values[i]);
        }
        if (pattern.matcher(query).find()) {
            throw new InvalidParameterException("Token numbers do not match function arguments");
        }

        return query;
    }

    @Override
    public String processSafeQuery(String query, int count) {
        for (int i = 0; i < count; i++) {
            query = replaceToken(query, i, Value.valueOf("?"));
        }
        if (pattern.matcher(query).find()) {
            throw new InvalidParameterException("Token numbers do not match function arguments");
        }

        return query;
    }

    @Override
    public List<String> getTokenList(String query) {

        ArrayList<String> matchList = new ArrayList<>();
        Matcher matcher = pattern.matcher(query);
        while (matcher.find()) {
            matchList.add(matcher.group());
        }

        ArrayList<String> tokenList = new ArrayList<>();
        for (String s : matchList) {
            tokenList.add(s.replaceAll(":", ""));
        }

        return tokenList;
    }

    private static String replaceToken(String query, int count, Value value) {
        String regex = ":[" + (count + 1) + "]";
        return query.replaceAll(regex, value.toString());
    }
}
