package com.comarch.function.sql.modifier;

import com.comarch.function.Value;

import java.util.List;

/**
 * Fills parametrized query with values;
 * Created by buzalskim on 2016-07-13.
 */
public interface SqlQueryModifier {

    String processSimpleQuery(String query, Value... values);

    String processSafeQuery(String query, int count);

    List<String> getTokenList(String query);

}
