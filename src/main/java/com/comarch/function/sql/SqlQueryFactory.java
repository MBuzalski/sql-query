package com.comarch.function.sql;


import com.google.inject.Inject;

import java.sql.Connection;

/**
 * Factory Method generating SqlQueryObjects
 * Created by buzalskim on 2016-07-14.
 */
public class SqlQueryFactory implements SqlQFactory {

    private SqlQueryFactory(){}

    @Override
    public SqlQuery create(String query) {
        return null;
    }

    public static SqlQuery create(Connection connection, String query, String which) {
        switch (which) {
            case "simple":
                return new SqlSimpleQuery(connection, query);
            case "safe":
                return new SqlSafeQuery(connection, query);
            default:
                return null;
        }
    }
}
