package com.comarch.function.sql;

import com.comarch.function.Function;
import com.comarch.function.sql.converter.DefaultResultSetToValueConverter;
import com.comarch.function.sql.converter.ResultSetToValueConverter;
import com.comarch.function.sql.modifier.DefaultSqlQueryModifier;
import com.comarch.function.sql.modifier.SqlQueryModifier;

import java.sql.Connection;


public abstract class SqlQuery implements Function {

    protected Connection connection;
    protected String query;
    private boolean used;
    protected SqlQueryModifier queryModifier;
    protected ResultSetToValueConverter resultSetToValueConverter;

    /**
     * @param connection - connection to the database query
     * @param query      - initial query string to process
     */
    public SqlQuery(Connection connection,
                    String query) {

        this.query = query;
        this.connection = connection;
        used = false;
        this.queryModifier = new DefaultSqlQueryModifier();
        this.resultSetToValueConverter = new DefaultResultSetToValueConverter();
    }

    public void setQueryModifier(SqlQueryModifier queryModifier) {
        this.queryModifier = queryModifier;
    }

    public void setResultSetToValueConverter(ResultSetToValueConverter resultSetToValueConverter) {
        this.resultSetToValueConverter = resultSetToValueConverter;
    }

    protected void alreadyUsed() {
        if (used) {
            throw new IllegalStateException("Invoke can only be used once per SqlQuery object");
        } else {
            used = true;
        }
    }
}
