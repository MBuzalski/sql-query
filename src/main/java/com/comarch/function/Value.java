package com.comarch.function;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public final class Value implements Serializable {
    public static final Value EMPTY_VALUE = new Value(null, null);
    private static final long serialVersionUID = -7590159781050610294L;
    private final Serializable content;
    private final Type type;

    public Value(Serializable value, Type type) {
        this.content = value;
        this.type = type;
    }

    public static Value valueOf(String str) {
        return new Value(str, Type.TEXT);
    }

    public static Value valueOf(Date dateCellValue) {
        return new Value(dateCellValue, Type.DATE);
    }

    public static Value valueOf(Serializable obj) {
        if (obj instanceof Number) {
            return new Value(obj, Type.NUMBER);
        } else if (obj instanceof Date) {
            return new Value(obj, Type.DATE);
        } else if (obj instanceof Boolean) {
            return new Value(obj, Type.BOOLEAN);
        } else if (obj instanceof String) {
            return new Value(obj, Type.TEXT);
        } else if (obj instanceof Value) {
            return (Value) obj;
        }
        return new Value(obj, Type.VALUE);
    }

    public static Value valueOf(double number) {
        Number num = BigDecimal.valueOf(number);
        return new Value(num, Type.NUMBER);
    }

    public static Value valueOf(String contentText, String format, Type type) throws ParseException {
        Value value;
        switch (type) {
            case NUMBER:
                value = Value.parseNumber(contentText);
                break;
            case BOOLEAN:
                value = Value.parseBoolean(contentText.toLowerCase());
                break;
            case DATE:
                value = Value.valueOf(contentText, format);
                break;
            default:
                value = new Value(contentText, type);
                break;
        }
        return value;
    }

    public static Value parseNumber(String number) throws ParseException {
        Number num = new BigDecimal(number);
        return new Value(num, Type.NUMBER);
    }

    public static Value valueOf(String date, String format) throws ParseException {
        Date dat = new SimpleDateFormat(format).parse(date);
        return new Value(dat, Type.DATE);
    }

    public static Value parseBoolean(String bool) {
        return new Value(Boolean.parseBoolean(bool), Type.BOOLEAN);
    }

    public boolean isValueList() {
        return content instanceof List;
    }

    public Serializable get() {
        return content;
    }

    public boolean isEmptyValue() {
        return equals(EMPTY_VALUE);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(content)
                .append(getType())
                .toHashCode();
    }

    public Type getType() {
        return type;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Value other = (Value) obj;
        if (content == null) {
            // jeżeli obie zawartości są null to typ nie ma znaczenia
            return other.content == null;
        } else if (!content.equals(other.content)) {
            return false;
        }
        return type == other.type;
    }

    @Override
    public String toString() {
        return String.valueOf(content);
    }

    public enum Type {
        TEXT, NUMBER, DATE, BOOLEAN, VALUE
    }

}
