package com.comarch.app;

import com.comarch.function.Value;
import com.comarch.function.sql.*;
import com.comarch.function.sql.modules.SqlSimpleQueryModule;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;

/**
 * •  W części where, zapytanie może zawierać tokeny oznaczające podstawienie argumentów wejściowych funkcji.
 * Format tokenu: :d+
 * Numeracja rozpoczyna się od 1, w przypadku braku ciągłości numeracji, powinien zostać zgłoszony wyjątek.
 * •  W przypadku braku wyników w SqlSimpleQuery.invoke zwrócić wartość Value.EMPTY
 * •  Na jednym obiekcie invoke może być zawołane co najwyżej raz
 * •  Zapytanie musi wiedzieć jakiego rodzaju parametr(y) pojawią się na wejściu i wyjściu
 */
public class Main {

    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);
    @Inject
    private static SqlQFactory factory;

    public static void main(String[] args) {

        Injector injector = Guice.createInjector(new SqlSimpleQueryModule());

        factory = injector.getInstance(SqlQFactory.class);
        Connection connection;
        try {
            connection = injector.getInstance(Connection.class);

            String query1 = "select tekst from testtable where liczba=:1";
            SqlQuery sQuery= factory.create(query1);

            System.out.println(sQuery.invoke(Value.valueOf(6)));


            connection.close();

        } catch (SQLException e) {
            LOGGER.error("{}: {}", e.getClass().getName(), e.getMessage());
            System.exit(0);
        }
    }
}
